let baseUrl = "https://v2.api-football.com"

function getHeaders (){
    return { headers: { 'X-RapidAPI-Key': '638f6e4309b5723981937f2c3a0012f9' }};
}

async function getCountries(){

    try {
        let request = await fetch(`${baseUrl}/countries`, getHeaders());
        let result = await request.json();

        return result.api.countries;
    } catch (e) { 
        console.log(e);
    }
     
}

async function getTeams(country) {
    let request = await fetch(`${baseUrl}/teams/search/${country}`, getHeaders());
    let result = await request.json();

    return result.api.teams;
}