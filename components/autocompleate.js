
async function createAutoCompleate(selector, refreshGrid = ()=> {}){
    var data = await getCountries();
    
    var auto = $(selector).kendoAutoComplete({
        dataTextField: "country",
        dataSource: data,
        filter: "startswith",
        select: function(e) {
            refreshGrid(e.dataItem.country);
        }
    });
}

