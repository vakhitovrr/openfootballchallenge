
let _widget = "";

function createGrid(selector){

    $(selector).kendoGrid({
        height: 550,
        pageable: {
            pageSizes: true,
            buttonCount: 5
        },
        columns: [{
            template: "<div class='logo' style='background-image: url(#:data.logo#);'></div>",
            field: "logo",
            title: "logo",
            width: 240
        }, {
            field: "name",
            title: "name"
        }, {
            field: "founded",
            title: "founded"
        }, {
            field: "venue_name",
            title: "venue_name"
        }, {
            field: "venue_surface",
            title: "venue_surface"
        }, {
            field: "venue_city",
            title: "venue_city"
        }]
    });

    _widget = $(selector).data("kendoGrid");
}

async function refreshGrid(country){
    const data = await getTeams(country);

    if (_widget)
    {
        _widget.dataSource.data(data);
        _widget.dataSource.pageSize(data.length);
    }
}


